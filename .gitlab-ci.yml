### Vault Demo CI ###
.envconsul: &envconsul
  - apk add --update curl jq
  - curl -s $VAULT_ADDR/v1/sys/seal-status
  - curl -s -O https://releases.hashicorp.com/envconsul/0.9.3/envconsul_0.9.3_linux_amd64.tgz
  - echo "fc48c15851119cf5bd6ecc609b6e2b15ece0c7b4d294c8d1890fddeae36a7aaa  envconsul_0.9.3_linux_amd64.tgz" |sha256sum -c
  - tar -xzvf envconsul_0.9.3_linux_amd64.tgz
  - install envconsul /usr/local/bin && rm envconsul
  - "export VAULT_LOGIN_OUTPUT=$(curl -s --request POST --data '{\"jwt\": \"'$GITOPS_DEMO_VAULT_JWT'\"}' $VAULT_ADDR/v1/auth/jwt/login)"
  - export VAULT_TOKEN=$(echo $VAULT_LOGIN_OUTPUT | jq -r '.auth.client_token')
  - export VAULT_ACCESSOR=$(echo $VAULT_LOGIN_OUTPUT | jq -r '.auth.accessor')
  - echo "Vault Token Accessor = $VAULT_ACCESSOR"
  - >- 
    if [ -z $VAULT_TOKEN ]; then
      echo "\$VAULT_TOKEN is empty, could be a bad login."
    else
      echo "\$VAULT_TOKEN is NOT empty, proceeding .."
      alias vaultrun="envconsul -config .vault/envconsul.hcl"
    fi

vault ping:
  image:
    name: hashicorp/vault:latest
    entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  stage: .pre
  script:
    - echo "Check status of $VAULT_ADDR"
    - |
      until vault status
      do
        echo "Vault returned error or sealed"
        sleep 5
      done
  rules:
    - if: '$VAULT_ADDR'
      when: always

Vault Client:
  image: 
    name: vault:latest
    entrypoint:
        - '/usr/bin/env'
        - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - vault status
  id_tokens:
    GITOPS_DEMO_VAULT_JWT:
      aud: "GITOPS-DEMO"
  script:
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login jwt=$GITOPS_DEMO_VAULT_JWT)"
    - export DEMO_NAME="$(vault kv get -field=DEMO_NAME secret/infrastructure/demo)"
    - export DEMO_DOMAIN="$(vault kv get -field=DEMO_DOMAIN secret/infrastructure/demo)"
    - ./test.sh
    - echo "Try to pull secret again. This should fail due to token expiration and no renewal."
    - vault kv get -field=DEMO_NAME secret/infrastructure/demo || true
      
Envconsul:
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  before_script:
    - *envconsul
  id_tokens:
    GITOPS_DEMO_VAULT_JWT:
      aud: "GITOPS-DEMO"
  script:
    - vaultrun ./test.sh
    - echo "Try to pull secret again. This should pass because envconsul takes care of the renewal."
  timeout: 15m

GitLab Secrets:
  id_tokens:
    GITOPS_DEMO_VAULT_JWT:
      aud: "GITOPS-DEMO"
  secrets:
    DEMO_NAME:
      vault: infrastructure/demo/DEMO_NAME@secret
      token: $GITOPS_DEMO_VAULT_JWT
    DEMO_DOMAIN:
      vault: infrastructure/demo/DEMO_DOMAIN@secret
      token: $GITOPS_DEMO_VAULT_JWT
  image:
    name: hashicorp/terraform:light
    entrypoint:
      - '/usr/bin/env'
      - 'PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin'
  script:
    - export DEMO_NAME=$(cat $DEMO_NAME)
    - export DEMO_DOMAIN=$(cat $DEMO_DOMAIN)
    - ./test.sh
    - echo "Secrets are only pulled at the begining of the job and written to a file, so no change in value will be detected."
    - echo "The Name is $DEMO_NAME and the Domain is $DEMO_DOMAIN"

AWS Secret Engine:
  image: 
    name: amazon/aws-cli:latest
    entrypoint: 
      - '/usr/bin/env'
  before_script:
    - curl -o vault.zip https://releases.hashicorp.com/vault/1.8.4/vault_1.8.4_linux_amd64.zip
    - yum -y install unzip jq
    - unzip vault.zip
    - install vault /usr/local/bin
    - vault status
  id_tokens:
    GITOPS_DEMO_VAULT_JWT:
      aud: "GITOPS-DEMO"
  script:
    - export VAULT_TOKEN="$(vault write -field=token auth/jwt/login jwt=$GITOPS_DEMO_VAULT_JWT)"
    - export AWS_CREDS="$(vault read -format=json aws/creds/deploy)"
    - export AWS_ACCESS_KEY_ID="$(echo $AWS_CREDS | jq -r '.data.access_key')"
    - export AWS_SECRET_ACCESS_KEY="$(echo $AWS_CREDS | jq -r '.data.secret_key')"
    - sleep 10
    - aws sts get-caller-identity